const { ipcRenderer } = require("electron");

window.addEventListener('DOMContentLoaded', function() {
    ipcRenderer.send("finishedLoading");
    
    document.getElementById("existingInstallBrowse").onclick = function() {
        ipcRenderer.send("browseExistingLocation");
    }

    document.getElementById("targetInstallBrowse").onclick = function() {
        ipcRenderer.send("browseTargetLocation");
    }

    document.getElementById("install").onclick = function() {
        ipcRenderer.send("startInstallation", document.getElementById("existingInstallPath").value, document.getElementById("targetInstallPath").value);
    }
});

ipcRenderer.on("updateExistingLocation", function(event, path) {
    document.getElementById("existingInstallPath").value = path;
});

ipcRenderer.on("updateTargetLocation", function(event, path) {
    document.getElementById("targetInstallPath").value = path;
});

ipcRenderer.on("setVerificationProgress", function(event, percentage) {
    document.getElementById("verifyProgressBackground").hidden = false;
    const progressBarForeground = document.getElementById("verifyProgressForeground");
    
    progressBarForeground.style.width = percentage + "%";
});

ipcRenderer.on("updateVerificationProgress", function(event, increment) {
    document.getElementById("verifyProgressBackground").hidden = false;
    const progressBarForeground = document.getElementById("verifyProgressForeground");

    const currentWidth = parseFloat(progressBarForeground.style.width);
    
    if (currentWidth >= 100)
        ipcRenderer.send("installationFinished");

    progressBarForeground.style.width = (currentWidth + parseFloat(increment)) + "%";
});