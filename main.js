const { app, BrowserWindow, Menu, shell, ipcMain, dialog } = require("electron");
const path = require("path")
const os = require("os");
const fs = require("fs");
const packageInfo = require("./package.json");
const log = require("electron-log");
const https = require("https");
const childProcess = require("child_process");
const crypto = require("crypto");

let mainWindow;

let TotalDownloadSize = 0;

const versionToEraList = {
	"0.0.140.646": "CU",
	"0.0.119.798": "Pre-CU",
    "0.0.203.189": "NGE",
    "1.0.0.1": "Legends"
};

function createWindow() {
	mainWindow = new BrowserWindow({
		width: 450,
        height: 210,
		webPreferences: {
            nodeIntegration: false,
			preload: path.join(__dirname, "/html/index_preload.js")
        },
        icon: path.join(__dirname, "/appIcon.ico")
	});

    mainWindow.loadFile("./html/index.html");

	mainWindow.on("closed", () => {
		mainWindow = null;
    });

	const menuTemplate = [
		{
			label: "Menu",
			submenu: [
				{
					role: "close"
				}
			]
		},
		{
			label: "View",
			submenu: [
				{
					role: "reload"
				},
				{
					role: "forcereload"
				}
			]
		},
		{
			role: "help",
			submenu: [
				{
					label: "Open Logs",
					click: () => {
						shell.openItem(os.homedir() + `\\AppData\\Roaming\\${packageInfo.name}\\log.log`);
					}
				},
				{
					label: "About",
					click: () => {
						aboutWindow = new BrowserWindow({
							width: 200,
							height: 300,
							title: "About",
							parent: mainWindow,
							modal: true,
							show: false,
							minimizable: false,
							maximizable: false,
							autoHideMenuBar: true,
							webPreferences: {
								nodeIntegration: false
							}
						});

						aboutWindow.loadFile("about.html");

						aboutWindow.once("ready-to-show", () => {
							aboutWindow.show();
						});

						aboutWindow.webContents.on("will-navigate", (event, address) => {
							event.preventDefault();
							shell.openExternal(address);
						});

						aboutWindow.on("closed", () => {
							aboutWindow = null;
						});
					}
				},
				/*{
					type: "separator"
                },
				{
					role: "toggledevtools"
                }*/
			]
		}
	];

	const appMenu = Menu.buildFromTemplate(menuTemplate);
    Menu.setApplicationMenu(appMenu);
}

app.on("ready", () => {
    createWindow();

    getTotalDownloadSize();
});

app.on("window-all-closed", () => {
	if (process.platform !== "darwin") app.quit();
});

app.on("activate", () => {
	if (mainWindow === null) createWindow();
});

ipcMain.on("browseExistingLocation", (event) => {
    const fileOptions = {
        title: "Find Existing Installation Executable",
        filters: [
			{ name: "Executable", extensions: ["exe"] }
		],
        defaultPath: "C:\\"
	}

    const filePath = dialog.showOpenDialog(mainWindow, fileOptions);

    try {
        childProcess.exec('wmic datafile where name="'+ filePath[0].split("\\").join("\\\\") + '" get Version', function(error, stdout, stderr) {
            if (!error) {
                const executableEra = versionToEraList[stdout.split("\n")[1].trim()];

                log.info(executableEra);

                mainWindow.webContents.send("updateExistingLocation", filePath[0]);

                // Verify installation to make sure it contains the files we need to copy
                verifyInstallation(filePath[0]);
            } else {
                log.error("Getting executable Version info from WMIC failed");
                //log.error(error);
            }
        });
    } catch (e) {
        log.error("No file selected for new executable");
        //log.error(e);
    }
});

function verifyInstallation(executablePath) {
    childProcess.exec('wmic datafile where name="'+ executablePath.split("\\").join("\\\\") + '" get Version', function(error, stdout, stderr) {
		if (!error) {
			let corruptedFiles = [];
			let missingFiles = [];

            const executableVersion = stdout.split("\n")[1].trim();
            let clientEra;

            try {
                clientEra = versionToEraList[executableVersion].toLowerCase();
            } catch (e) {
                dialog.showMessageBox(mainWindow, {
                    type: "error",
                    title: "Unknown Version Detected",
                    message: "The version of Star Wars Galaxies that you are trying to use as a base is unknown. Please contact the CUEmu staff for assistance.",
                    buttons: ["Ok"]
                });

                mainWindow.webContents.executeJavaScript("document.getElementById('existingInstallPath').value = '';");

                return;
            }
            
            if (clientEra == "pre-cu") {
                dialog.showMessageBox(mainWindow, {
                    type: "error",
                    title: "Improper Version Detected",
                    message: "You cannot use a Pre-CU installation as a base for CUEmu. Please find a legal installation that is of the NGE era and try again.",
                    buttons: ["Ok"]
                });

                mainWindow.webContents.executeJavaScript("document.getElementById('existingInstallPath').value = '';");

                return;
            }

            mainWindow.setTitle(mainWindow.getTitle() + " - Verifying Installation");

            const requiredFiles = require("./requiredFiles.json"); // Checksums use the MD5 algorithm with a Hexidecimal (Base16) encoding
            const baseFiles = Object.keys(requiredFiles);
        
            for (let i = 0; i < baseFiles.length; i++) {
                log.info("Verifying file: " + baseFiles[i]);
        
                if (fs.existsSync(path.dirname(executablePath) + "\\" + baseFiles[i])) {
                    const fileBuffer = fs.readFileSync(path.dirname(executablePath) + "\\" + baseFiles[i]);
                    const generatedHash = crypto.createHash("md5").update(fileBuffer).digest("hex");
                    const validHash = requiredFiles[baseFiles[i]];
                    
                    mainWindow.webContents.send("setVerificationProgress", progress(baseFiles.length));
        
                    if (generatedHash.toLowerCase() != validHash.toLowerCase()) {
                        log.error("\tCorrupted");
                        corruptedFiles.push(baseFiles[i]);
                    }
                } else {
                    log.error("\tMissing");
                    missingFiles.push(baseFiles[i]);
                }
            }

            mainWindow.webContents.executeJavaScript("document.getElementById('verifyProgressBackground').hidden = true;");
            mainWindow.webContents.executeJavaScript(`document.getElementById("verifyProgressForeground").style.width = "0%"`);
            mainWindow.setTitle("CUEmu Installer");

            if ((corruptedFiles.length + missingFiles.length) > 0) {
                dialog.showMessageBox(mainWindow, {
                    type: "error",
                    title: "Existing Installation Error",
                    message: "This installation is either corrupted or incomplete. Please choose another installation or repair the selected one.",
                    buttons: ["Ok"]
                });
            } else {
                mainWindow.webContents.executeJavaScript("document.getElementById('targetInstallPath').removeAttribute('disabled');");
                mainWindow.webContents.executeJavaScript("document.getElementById('targetInstallBrowse').removeAttribute('disabled');");
            }
		} else {
            log.error("Getting executable Version info from WMIC failed");
            //log.error(error);
        }
	});
}

ipcMain.on("browseTargetLocation", (event) => {
    const fileOptions = {
        title: "Locate Target Installation Folder",
        properties: ['openDirectory'],
        defaultPath: "C:\\"
	}

    const filePath = dialog.showOpenDialog(mainWindow, fileOptions);

    // Based on the Browse functionality, we assume the path exists as it will not let you select a non-existant folder; we add in a check later in case they add to the input field

    try {
        mainWindow.webContents.send("updateTargetLocation", filePath[0]);
    } catch (e) {
        log.error("No folder selected for target location");
        //log.error(e);
    }

    mainWindow.webContents.executeJavaScript("document.getElementById('install').removeAttribute('disabled');");
});

ipcMain.on("startInstallation", (event, fromPath, toPath) => {
    log.info(toPath);

    if (!fs.existsSync(toPath)) {
        dialog.showMessageBox(mainWindow, {
            type: "question",
            title: "Target Location Invalid",
            message: "Target installation path does not exist. Would you like to create it?",
            buttons: ["Yes", "No"]
        }, (response) => {
            log.info(response);

            if (response == 0) {
                fs.mkdirSync(toPath);
            } else
                return;
        });
    }

    mainWindow.webContents.executeJavaScript("document.getElementById('targetInstallPath').setAttribute('disabled', true);");
    mainWindow.webContents.executeJavaScript("document.getElementById('targetInstallBrowse').setAttribute('disabled', true);");
    mainWindow.webContents.executeJavaScript("document.getElementById('existingInstallPath').setAttribute('disabled', true);");
    mainWindow.webContents.executeJavaScript("document.getElementById('existingInstallBrowse').setAttribute('disabled', true);");
    mainWindow.webContents.executeJavaScript("document.getElementById('install').setAttribute('disabled', true);");

    mainWindow.setTitle(mainWindow.getTitle() + " - Installing");

    const requiredJson = require("./requiredFiles.json");
    const requiredBaseList = Object.keys(requiredJson);

    const downloadJson = require("./downloadFiles.json");
    const downloadMilesList = Object.keys(downloadJson.miles);
    const downloadBaseList = Object.keys(downloadJson.base);
	const downloadCUEmuList = Object.keys(downloadJson.cuemu);
    
    fs.mkdirSync(toPath + "\\miles");

    for (const nextFile of requiredBaseList) {
        log.info("Copying file: " + nextFile);

        if (downloadMilesList.includes(nextFile) || downloadBaseList.includes(nextFile))
            continue;

        copyFile(path.dirname(fromPath) + "\\" + nextFile, toPath + "\\" + nextFile);
    };

    for (const nextFile of downloadMilesList) {
        const destination = toPath + "\\miles\\" + nextFile;

        downloadFile(downloadJson.miles[nextFile], destination);
    };

    for (const nextFile of downloadBaseList) {
        const destination = toPath + "\\" + nextFile;

        downloadFile(downloadJson.base[nextFile], destination);
    };

	for (const nextFile of downloadCUEmuList) {
        const destination = toPath + "\\" + nextFile;

        downloadFile(downloadJson.cuemu[nextFile], destination);
    };
});

ipcMain.on("installationFinished", (event) => {
    if (mainWindow.getTitle() != "CUEmu Installer") {
        mainWindow.setTitle("CUEmu Installer");

        dialog.showMessageBox(mainWindow, {
            type: "info",
            title: "Installation Complete",
            message: "CUEmu has been successfully installed.",
            buttons: ["Ok"]
        }, () => {
            app.quit();
        });
    }
});

function copyFile(source, destination) {
    const readStream = fs.createReadStream(source);
    const writeStream = fs.createWriteStream(destination, {flags: "a"});

    readStream.pipe(writeStream);

    readStream.on("error", (error) => {
        readStream.close();
        log.info(error);
    });

    readStream.on("end", () => {
        readStream.close();
    });

    writeStream.on("error", (error) => {
        writeStream.end();
        log.error(error);
    });

    writeStream.on("finish", () => {
        writeStream.end();
    });
}

function progress(total) {
    this.inc = (total !== 0 ? this.inc || 0 : -1) + 1;

    var percentage = Math.floor(this.inc / total * 100);

    if (percentage >= 100)
        this.inc = 0;
    
    return percentage;
}

function downloadFile(downloadUrl, destination) {
    const request = https.get(downloadUrl, (response) => {
        if (response.statusCode > 300 && response.statusCode < 400 && response.headers.location) {
            return downloadFile(response.headers.location, destination);
        } else if (response.statusCode !== 200) {
            log.error("Response status was " + response.statusCode + " for " + downloadUrl)
            return;
        }

        const fileWriter = fs.createWriteStream(destination, {flags: "a"});

        response.pipe(fileWriter);

        response.on("data", (chunk) => {
            mainWindow.webContents.send("updateVerificationProgress", chunk.length / TotalDownloadSize * 100); // Total size should be 69783384
        });

        fileWriter.on("error", (error) => {
            fileWriter.end();
            log.error(error);
        });

        fileWriter.on("finish", () => {
            fileWriter.end();
        });
    });

    request.on("error", (error) => {
        log.error(error);
    });
}

function getTotalDownloadSize() {
    const downloadList = require("./downloadFiles.json");

    for (const nextLevel of Object.keys(downloadList)) {
        for (const [fileName, downloadUrl] of Object.entries(downloadList[nextLevel])) {
            const getFileInformation = (theUrl) => {
                const request = https.get(theUrl, (response) => {
                    if (response.statusCode > 300 && response.statusCode < 400 && response.headers.location) {
                        return getFileInformation(response.headers.location);
                    } else if (response.statusCode !== 200) {
                        log.error("Response status was " + response.statusCode + " for " + theUrl)
                        return;
                    }

                    TotalDownloadSize += parseInt(response.headers["content-length"]);
                });

                request.on("error", (error) => {
                    log.error(error);
                });
            }

            getFileInformation(downloadUrl);
        }
    }
}

ipcMain.on("finishedLoading", () => {
    dialog.showMessageBox(mainWindow, {
        type: "info",
        title: "Instructions",
        message: "Welcome to the CUEmu Installer",
        detail: "To begin, you must have a working installation of a retail version of Star Wars Galaxies from the NGE-era.\n\nSelect the executable for the first browse location, and then select a new location to install CUEmu into. After the installation you will be able to load up the game and connect to CUEmu's server.",
        buttons: ["Ok"]
    });
});